<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h4 class="mt-4"><?php echo $titulo; ?></h4>
            <?php \Config\Services::validation()->listErrors(); ?>
            <form action="<?php echo base_url(); ?>/productos/actualizar" method="post" autocomplete="off">
                <?php csrf_field(); ?>
                <input type="hidden" name="id" id="id" value="<?php echo $producto['id']; ?>">
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <label for="codigo">Código</label>
                            <input type="text" class="form-control" id="codigo" name="codigo" autofocus required value="<?php echo $producto['codigo']; ?>">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" required value="<?php echo $producto['nombre']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <label for="id_unidad">Unidad</label>
                            <select class="form-control" name="id_unidad" id="id_unidad" required>
                                <option value="">Seleccionar unidad</option>
                                <?php foreach ($unidades as $unidad) { ?>
                                    <option value="<?php echo $unidad['id']; ?>" <?php if($unidad['id'] == $producto['id_unidad']) echo 'selected' ?>><?php echo $unidad['nombre']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="id_categoria">Categoría</label>
                            <select class="form-control" name="id_categoria" id="id_categoria" required>
                                <option value="">Seleccionar categoría</option>
                                <?php foreach ($categorias as $categoria) { ?>
                                    <option value="<?php echo $categoria['id']; ?>" <?php if($categoria['id'] == $producto['id_categoria']) echo 'selected' ?>><?php echo $categoria['nombre']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <label for="precio_venta">Precio venta</label>
                            <input type="text" class="form-control" id="precio_venta" name="precio_venta" required value="<?php echo $producto['precio_venta']; ?>">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="precio_compra">Precio compra</label>
                            <input type="text" class="form-control" id="precio_compra" name="precio_compra" required value="<?php echo $producto['precio_compra']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <label for="stock_minimo">Stock mínimo</label>
                            <input type="text" class="form-control" id="stock_minimo" name="stock_minimo" required value="<?php echo $producto['stock_minimo']; ?>">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="precio_compra">Es inventariable</label>
                            <select class="form-control" name="inventariable" id="inventariable">
                                <option value="1" <?php if($producto['inventariable'] == 1) echo 'selected' ?>>Sí</option>
                                <option value="0" <?php if($producto['inventariable'] == 0) echo 'selected' ?>>No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <a href="<?php echo base_url(); ?>/productos" class="btn btn-primary">Regresar</a>
                <button type="submit" class="btn btn-success">Guardar</button>
            </form>
        </div>
    </main>