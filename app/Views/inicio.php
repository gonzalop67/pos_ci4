<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">

            <br>

            <div class="row">
                <div class="col-4">
                    <div class="card text-white bg-primary">
                        <div class="card-body">
                            <?php echo $total; ?> Total de productos
                        </div>

                        <a href="<?php echo base_url() ?>/productos" class="card-footer text-white">Ver detalles</a>
                    </div>
                </div>

                <div class="col-4">
                    <div class="card text-white bg-success">
                        <div class="card-body">
                            <?php echo $totalVentas['total']; ?> Ventas del día
                        </div>

                        <a href="<?php echo base_url() ?>/ventas" class="card-footer text-white">Ver detalles</a>
                    </div>
                </div>

                <div class="col-4">
                    <div class="card text-white bg-danger">
                        <div class="card-body">
                            <?php echo $minimos; ?> Productos con stock mínimo
                        </div>

                        <a href="<?php echo base_url() ?>/productos/mostarMinimos" class="card-footer text-white">Ver detalles</a>
                    </div>
                </div>

            </div>
            
            <div class="row">
                <div class="col-4">
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>

                <div class="col-4">
                    <a href="<?php echo base_url(); ?>/inicio/excel" class="btn btn-primary">Genera Excel</a>
                </div>
            </div>

        </div>
    </main>

    <script>
        const ctx = document.getElementById('myChart').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>